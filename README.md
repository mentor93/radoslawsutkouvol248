Czy warto w zadaniu wykorzystać object pooling?

W pierwszym zadaniu, z procą a'la Angry Birds, raczej nie warto, jako że tworzymy tam jedną nową instancję kulki co nawet kilka sekund. Object pooling jest przede wszystkim przydatny wówczas, kiedy mamy do stworzenia dużo obiektów naraz (np. pociski w shmupie), więc tutaj raczej nieszczególnie warto się za to zabierać.

W drugim zadaniu natomiast zdecydowanie warto by zrobić object pooling. Tworzymy tam nowe obiekty często (co ułamek sekundy), plus mamy sytuację, kiedy duża kulka rozbija się na wiele mniejszych kulek. Prawdę powiedziawszy, po prostu jednak nie chce mi się już tego robić :/