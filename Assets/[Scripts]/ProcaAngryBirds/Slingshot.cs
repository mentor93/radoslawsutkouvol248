﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slingshot : MonoBehaviour
{
    private static Slingshot _instance;
    public static Slingshot Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<Slingshot>();
            return _instance;
        }

        set => _instance = value;
    }

    private Vector2 stretchStartPosition = Vector2.zero;
    private Vector2 stretchCurrentPosition = Vector2.zero;

    private bool ballHasLaunched = false;
    private float ballSpeedStopThreshold = 10.0f;
    private Ball ball;

    private Coroutine waitForBallToStopCoroutine = null;

    [SerializeField] private float maxStretch = 3.5f;
    [SerializeField] private float shootForce = 20.0f;
    [SerializeField] private Transform slingshotTop;
    [SerializeField] private GameObject ballPrefab;

    void Start()
    {
        SpawnBall();
    }

    void Update()
    {
        if (ball == null)
            return;

        if (!ballHasLaunched)
        {
            if (Input.GetMouseButtonDown(0))
            {
                stretchStartPosition = GetCursorInWorldPosition();
            }

            if (Input.GetMouseButton(0))
            {
                UpdateBeforeLaunchPosition();
            }

            if (Input.GetMouseButtonUp(0))
            {
                Shoot();
            }
        }
    }

    Vector2 GetCursorInWorldPosition()
    {
        return Camera.main.ScreenToWorldPoint(Input.mousePosition);
    }

    void UpdateBeforeLaunchPosition()
    {
        stretchCurrentPosition = GetCursorInWorldPosition();

        if (Vector2.Distance(stretchCurrentPosition, slingshotTop.position) > maxStretch)
        {
            stretchCurrentPosition = (stretchCurrentPosition - new Vector2(slingshotTop.position.x, slingshotTop.position.y)).normalized * maxStretch
                + new Vector2(slingshotTop.position.x, slingshotTop.position.y);
        }

        ball.transform.position = stretchCurrentPosition;
    }

    void Shoot()
    {
        Vector2 direction = new Vector2(slingshotTop.position.x, slingshotTop.position.y) - new Vector2(ball.transform.position.x, ball.transform.position.y);
        float distance = Vector2.Distance(ball.transform.position, slingshotTop.position);

        ball.GetComponent<Ball>().Throw(direction, shootForce, distance);

        CameraController.Instance.Follow(ball.transform);

        ballHasLaunched = true;

        if (waitForBallToStopCoroutine != null)
        {
            StopCoroutine(waitForBallToStopCoroutine);
        }

        waitForBallToStopCoroutine = StartCoroutine(WaitForBallToStop());
    }

    void SpawnBall()
    {
        ball = Instantiate(ballPrefab, slingshotTop.position, Quaternion.identity).GetComponent<Ball>();
    }

    IEnumerator WaitForBallToStop()
    {
        while (!ball.HasStopped)
        {
            yield return null;
        }

        yield return new WaitForSeconds(1.0f);

        SpawnBall();
        CameraController.Instance.ReturnToStartPosition();
        ballHasLaunched = false;
    }
}
