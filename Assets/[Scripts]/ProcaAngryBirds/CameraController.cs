﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    private static CameraController _instance;
    public static CameraController Instance {
        get {
            if (_instance == null)
                _instance = FindObjectOfType<CameraController>();
            return _instance;
        }

        set => _instance = value;
    }

    private Vector3 startPosition = Vector3.zero;
    private Transform followTarget = null;

    [SerializeField] private float followSpeed = 10.0f;

    void Start()
    {
        startPosition = transform.position;
    }

    void Update()
    {
        if (followTarget != null)
        {
            if (Camera.main.WorldToScreenPoint(followTarget.transform.position).x > Screen.width / 2.0f)
            {
                transform.position = new Vector3(followTarget.position.x, transform.position.y, transform.position.z);
            }
        }
    }

    public void Follow(Transform target)
    {
        followTarget = target;
    }

    public void ReturnToStartPosition()
    {
        followTarget = null;
        transform.position = startPosition;
    }
}
