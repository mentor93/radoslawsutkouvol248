﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Ball : MonoBehaviour
{
    private bool hasTouchedGround = false;

    private float ballSpeedStopThreshold = 3.0f;

    private Rigidbody2D rigidbody;

    public float Speed => rigidbody.velocity.magnitude / Time.fixedDeltaTime;

    public bool HasStopped => hasTouchedGround && Speed <= ballSpeedStopThreshold;

    void Awake()
    {
        rigidbody = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        
    }

    public void Throw(Vector2 direction, float shootForce, float distanceToSlingshot)
    {
        if (rigidbody == null) return;

        rigidbody.bodyType = RigidbodyType2D.Dynamic;
        rigidbody.AddForce(direction * shootForce * distanceToSlingshot * Time.deltaTime);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            hasTouchedGround = true;
        }
    }
}
