﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UI : MonoBehaviour
{
    private static UI _instance;
    public static UI Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<UI>();

            return _instance;
        }

        set => _instance = value;
    }

    [SerializeField] private TextMeshProUGUI totalBallsText;

    void Start()
    {
        
    }

    void Update()
    {
        UpdateTotalBallsText();
    }

    public void UpdateTotalBallsText()
    {
        totalBallsText.text = "Total balls: " + BallSpawner.Instance.TotalBalls;
    }
}
