﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallSpawner : MonoBehaviour
{
    private static BallSpawner _instance;
    public static BallSpawner Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<BallSpawner>();

            return _instance;
        }

        set => _instance = value;
    }

    private Coroutine spawnBallsCoroutine = null;
    private float spawnBallsInterval = 0.25f;
    private int maxBalls = 250;
    private GameObject ballInstance = null;
    private float cameraXEdgePosition = 0.0f;
    private float cameraYEdgePosition = 0.0f;

    public GameObject ballPrefab;

    public int TotalBalls { get { return transform.childCount; } }

    void Start()
    {
        if (spawnBallsCoroutine != null)
            StopCoroutine(spawnBallsCoroutine);

        spawnBallsCoroutine = StartCoroutine(SpawnBalls());

        cameraXEdgePosition = Camera.main.orthographicSize * Screen.width / Screen.height;
        cameraYEdgePosition = -Camera.main.orthographicSize;
    }

    void Update()
    {
        
    }

    IEnumerator SpawnBalls()
    {
        while (TotalBalls < maxBalls)
        {
            SpawnBallAtEmptySpace();
            yield return new WaitForSeconds(spawnBallsInterval);
        }

        GravitationBall ball = null;
        foreach (Transform child in transform)
        {
            ball = child.GetComponent<GravitationBall>();
            if (ball) ball.InvertGravityPull();
        }
    }

    void SpawnBallAtEmptySpace()
    {
        float xPosition = Random.Range(-cameraXEdgePosition, cameraXEdgePosition);
        float yPosition = Random.Range(-cameraYEdgePosition, cameraYEdgePosition);
        ballInstance = Instantiate(ballPrefab, new Vector3(xPosition, yPosition, 0.0f), Quaternion.identity, transform);
    }
}
