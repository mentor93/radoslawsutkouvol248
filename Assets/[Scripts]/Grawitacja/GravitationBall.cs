﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class GravitationBall : MonoBehaviour
{
    private Coroutine simulateGravityFieldCoroutine = null;
    private float gravityFieldSimulationInterval = 0.1f;

    private const int maxOtherBalls = 10;
    private Collider2D[] otherBalls = new Collider2D[maxOtherBalls];
    //private GravitationBall otherBall = null;
    private int howManyBallsAreInsideMe = 1;

    private Coroutine turnCollisionCoroutine = null;
    private float dissolveCollisionOffDelay = 0.5f;
    private bool isDissolving = false;

    public float gravityFieldRadius = 6.0f;
    [SerializeField] private LayerMask gravitationBallLayerMask;
    [SerializeField] private float gravityPullForce = 3.0f;
    [SerializeField] private float dissolveForce = 120.0f;
    [HideInInspector] public Rigidbody2D rigidbody;
    [SerializeField] private bool showDebug = false;

    [HideInInspector] public bool inMerge = false;

    public float Mass { get { return rigidbody.mass; } }

    void Awake()
    {
        rigidbody = GetComponent<Rigidbody2D>();

        if (simulateGravityFieldCoroutine != null)
            StopCoroutine(simulateGravityFieldCoroutine);

        simulateGravityFieldCoroutine = StartCoroutine(SimulateGravityField());
    }

    void Update()
    {

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (inMerge) return;

        GravitationBall otherBall = collision.gameObject.GetComponent<GravitationBall>();

        if (otherBall)
        {
            otherBall.RequestMerge(this);
        }
    }

    IEnumerator SimulateGravityField()
    {
        while (true)
        {
            int numOfOtherBalls = Physics2D.OverlapCircleNonAlloc(transform.position, gravityFieldRadius, otherBalls, gravitationBallLayerMask);

            if (numOfOtherBalls > 1)
            {
                for (int i = 0; i < numOfOtherBalls; i++)
                {
                    GravitationBall otherBall = otherBalls[i].GetComponent<GravitationBall>();

                    if (otherBall)
                    {
                        Vector2 direction = ((Vector2)(transform.position - otherBall.transform.position)).normalized;
                        otherBall.GravityPull(direction * gravityPullForce);
                    }
                }
            }

            yield return new WaitForSeconds(gravityFieldSimulationInterval);
        }
    }

    public void GravityPull(Vector2 force)
    {
        if (rigidbody != null)
            rigidbody.AddForce(force * rigidbody.mass);
    }

    public void RequestMerge(GravitationBall otherBall)
    {
        if (inMerge || !rigidbody) return;

        inMerge = true;
        otherBall.inMerge = true;
        otherBall.Merge(transform.localScale, rigidbody.mass, gravityFieldRadius, howManyBallsAreInsideMe);
        gameObject.SetActive(false);
        Destroy(gameObject);
    }

    public void Merge(Vector3 scale, float mass, float gravityRadius, int howManyBalls)
    {
        transform.localScale += scale;
        rigidbody.mass += mass;
        gravityFieldRadius += gravityRadius;

        howManyBallsAreInsideMe += howManyBalls;

        inMerge = false;

        if (rigidbody.mass >= 50 && !isDissolving)
        {
            Dissolve();
        }
    }

    void Dissolve()
    {
        isDissolving = true;
        GravitationBall dissolvedBall = null;
        for (int i=0; i<howManyBallsAreInsideMe; i++)
        {
            dissolvedBall = Instantiate(BallSpawner.Instance.ballPrefab, transform.position + Random.onUnitSphere * 10.0f, Quaternion.identity).GetComponent<GravitationBall>();
            dissolvedBall.transform.SetParent(BallSpawner.Instance.transform);
            dissolvedBall.rigidbody.mass = 1;
            dissolvedBall.transform.localScale = new Vector3(4.0f, 4.0f, 4.0f);
            dissolvedBall.rigidbody.velocity = Random.onUnitSphere * dissolveForce;
            dissolvedBall.TurnCollisionAfterDelay(dissolveCollisionOffDelay);
        }
        gameObject.SetActive(false);
        Destroy(gameObject);
    }

    public void InvertGravityPull()
    {
        gravityPullForce *= -1;
    }

    public void TurnCollisionAfterDelay(float delay)
    {
        if (turnCollisionCoroutine != null)
            StopCoroutine(turnCollisionCoroutine);

        turnCollisionCoroutine = StartCoroutine(TurnCollision(delay));
    }

    IEnumerator TurnCollision(float delay)
    {
        gameObject.name = "dissolvedBall" + Random.Range(10000, 100000); 
        GetComponent<Collider2D>().enabled = false;
        yield return new WaitForSeconds(delay);
        GetComponent<Collider2D>().enabled = true;
    }
}
